# [0.2.0] - 2023-12-18

- Utilisation de l'API iparapheur en version 5.0.20

# [0.1.0] - 2023-01-03

## Ajout

- Génération des fonctions GET, POST et DELETE