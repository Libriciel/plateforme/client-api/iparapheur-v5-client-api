# Utilisation

```
composer require libriciel/iparapheur-v5-client-api
```

Voir les exemples dans le repertoires `exemples`. Il est nécessaire de créer un fichier `.env` dans ce répertoire avec les données de connexion du iparapheur.
Voir le fichier `.env.dist` pour la configuration du fichier `.env`

# Développement

Voir le Makefile


# Biographie

- Swagger du iparapheur v5 : https://iparapheur-5-0.partenaire.libriciel.fr/api/swagger-ui/index.html#/
- Utilisation de PSR-7, 17 et 18 : https://github.com/KnpLabs/php-github-api/tree/master/lib/Github/Api
