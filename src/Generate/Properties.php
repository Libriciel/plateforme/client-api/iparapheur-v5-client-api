<?php

namespace IparapheurV5Client\Generate;

class Properties
{
    public string $name;
    public string $type;
    public ?string $subType = null;
}
