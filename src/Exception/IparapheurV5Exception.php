<?php

declare(strict_types=1);

namespace IparapheurV5Client\Exception;

use Http\Client\Exception;

final class IparapheurV5Exception extends \Exception implements Exception
{
}
