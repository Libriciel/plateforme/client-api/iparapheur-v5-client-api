<?php

namespace IparapheurV5Client\Model;

class SortObject
{
    public bool $unsorted;
    public bool $sorted;
    public bool $empty;
}