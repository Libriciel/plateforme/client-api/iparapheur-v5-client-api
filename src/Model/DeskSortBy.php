<?php

namespace IparapheurV5Client\Model;

enum DeskSortBy : string
{
    case NAME = 'NAME';
    case ID = 'ID';
}