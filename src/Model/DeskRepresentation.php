<?php

namespace IparapheurV5Client\Model;

class DeskRepresentation
{
    public string $id;
    public string $name;
    public string $tenantId;
}