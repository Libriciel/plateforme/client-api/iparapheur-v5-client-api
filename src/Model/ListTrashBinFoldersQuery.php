<?php

namespace IparapheurV5Client\Model;

class ListTrashBinFoldersQuery
{
    public int $page;
    public int $size;
    /** @var string[] */
    public array $sort;
}
