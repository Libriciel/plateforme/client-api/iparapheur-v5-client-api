<?php

namespace IparapheurV5Client\Model;

enum TypeSortBy : string
{
    case NAME = 'NAME';
    case ID = 'ID';
}