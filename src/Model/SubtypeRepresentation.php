<?php

namespace IparapheurV5Client\Model;

class SubtypeRepresentation
{
    public string $id;
    public string $name;
}