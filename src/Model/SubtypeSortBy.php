<?php

namespace IparapheurV5Client\Model;

enum SubtypeSortBy : string
{
    case NAME = 'NAME';
    case ID = 'ID';
}