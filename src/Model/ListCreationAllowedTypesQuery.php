<?php

namespace IparapheurV5Client\Model;

class ListCreationAllowedTypesQuery
{
    public int $page;
    public int $size;
    /** @var string[] */
    public array $sort;
}
