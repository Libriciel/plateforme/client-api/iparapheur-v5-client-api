<?php

namespace IparapheurV5Client\Model;

class SimpleTaskParams
{
    public string $publicAnnotation;
    public string $privateAnnotation;
    /** @var string[] */
    public array $metadata;
}