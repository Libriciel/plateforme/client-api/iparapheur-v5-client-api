<?php

namespace IparapheurV5Client\Model;

class TenantRepresentation
{
    public string $id;
    public string $name;
}