<?php

namespace IparapheurV5Client\Model;

class ListTypesQuery
{
    public int $page;
    public int $size;
    /** @var string[] */
    public array $sort;
}
