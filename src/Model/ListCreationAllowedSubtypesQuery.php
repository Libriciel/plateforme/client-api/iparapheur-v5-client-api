<?php

namespace IparapheurV5Client\Model;

class ListCreationAllowedSubtypesQuery
{
    public int $page;
    public int $size;
    /** @var string[] */
    public array $sort;
}
