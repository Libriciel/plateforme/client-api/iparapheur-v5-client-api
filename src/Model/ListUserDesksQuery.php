<?php

namespace IparapheurV5Client\Model;

class ListUserDesksQuery
{
    public int $page;
    public int $size;
    /** @var string[] */
    public array $sort;
}
