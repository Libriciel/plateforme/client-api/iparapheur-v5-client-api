<?php

namespace IparapheurV5Client\Model;

class ListSubtypesQuery
{
    public int $page;
    public int $size;
    /** @var string[] */
    public array $sort;
}
