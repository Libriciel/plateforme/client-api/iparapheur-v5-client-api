<?php

namespace IparapheurV5Client\Model;

class TypeRepresentation
{
    public string $id;
    public string $name;
    public string $description;
}