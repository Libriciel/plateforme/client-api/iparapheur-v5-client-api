<?php

namespace IparapheurV5Client\Model;

enum TenantSortBy : string
{
    case NAME = 'NAME';
    case ID = 'ID';
}